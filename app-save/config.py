import os

#config database
POSTGRES_USER = os.environ['POSTGRES_USER']
POSTGRES_PASSWORD = os.environ['POSTGRES_PASSWORD']
POSTGRES_DB = os.environ['POSTGRES_DB']
POSTGRES_HOST = os.environ['POSTGRES_HOST']
sqlalchemy_database_uri = 'postgresql://' + POSTGRES_USER + ':' + \
                        POSTGRES_PASSWORD + '@' + POSTGRES_HOST + '/' + POSTGRES_DB


#config broker mqtt
broker_mqtt = os.environ['BROKER_MQTT']
port_mqtt = os.environ.get('PORT_MQTT', 1883)
sub_topic = os.environ.get('SUB_TOPIC', "huertechno/#") 
