from sqlalchemy.sql.expression import func
from sqlalchemy.orm import relationship
from app.common.database import Base
from sqlalchemy import  (Table, Column,
    Integer, DateTime, ForeignKey, String)




class BaseDb(Base):

    __abstract__ = True
    id = Column(Integer, primary_key=True)
    date_created  = Column(DateTime,  default=func.current_timestamp())

    def _asdict(self):
        result = dict()
        for key in self.__mapper__.c.keys():
            result[key] = getattr(self, key)
        return result


cultivo_device = Table('cultivo_device', Base.metadata,
    Column('cultivo_id', Integer, ForeignKey('cultivo.id')),
    Column('device_id', Integer, ForeignKey('device.id'))
)



device_component = Table('device_component', Base.metadata,
    Column('device_id', Integer, ForeignKey('device.id')),
    Column('component_id', Integer, ForeignKey('component.id'))
)




class Cultivo(BaseDb):

    __tablename__ = 'cultivo'
    name = Column(String)
    type_cultivo = Column(String)
    state = Column(String)
    device = relationship(
        "Device",
        secondary = cultivo_device,
        back_populates = "cultivo")


    def __init__(self, name, type_cultivo, state):
        self.name = name
        self.type_cultivo = type_cultivo
        self.state = state


class Device(BaseDb):

    __tablename__ = 'device'
    name = Column(String)
    model = Column(String)
    marca = Column(String)
    state = Column(String)
    cultivo = relationship(
        "Cultivo",
        secondary = cultivo_device,
        back_populates = "device")
    component = relationship(
        "Component",
        secondary = device_component,
        back_populates = "device")


    def __init__(self, name, model, marca, state):
        self.name = name
        self.model = model
        self.marca = marca
        self.state = state


class Component(BaseDb):

    __tablename__ = 'component'
    name = Column(String)
    model = Column(String)
    variable = Column(String)
    function = Column(String)
    state = Column(String)
    num_pin = Column(String)
    measure = relationship("Measure", backref="component")
    device = relationship(
        "Device",
        secondary = device_component,
        back_populates = "component")

    def __init__(self, name, model, variable, function,
    state, num_pin):
        self.name = name
        self.model = model
        self.variable = variable
        self.function = function
        self.state = state
        self.num_pin = num_pin



class Measure(BaseDb):

    __tablename__= 'measure'

    value = Column(String, nullable=False)
    component_id = Column(Integer, ForeignKey('component.id'))

    def __init__(self, value, component_id):
        self.value = value
        self.component_id = component_id




