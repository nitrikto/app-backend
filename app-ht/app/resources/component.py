from flask_restful import reqparse, Resource
from app.models import Device as DeviceDb
from app.models import Component as ComponentDb
from app.common.database import session


parser_component = reqparse.RequestParser()
parser_component.add_argument('id')
parser_component.add_argument('name')
parser_component.add_argument('model')
parser_component.add_argument('variable')
parser_component.add_argument('function')
parser_component.add_argument('state')
parser_component.add_argument('num_pin')
parser_component.add_argument('device')


class Component(Resource):

    def get(self):
        try:
            components = [c._asdict() for c in session.query(ComponentDb).all()]
        except:
            return str("No se encuentran componentes registrados")
        return jsonify(components)

    def post(self):
        args_component = parser_component.parse_args()
        device_args = args_component['device'].split(';')
        devices = [session.query(DeviceDb) \
        .filter_by(id=d_id) \
        .first() for d_id in device_args]
        component = ComponentDb(args_component['name'], \
            args_component['model'], \
            args_component['variable'], \
            args_component['function'], \
            args_component['state'], \
            args_component['num_pin'])
        component.device = devices
        session.add(component)
        session.commit()
        return dict(args_component)

    def delete(self):
        args_component = parser_component.parse_args()
        del_component = session.query(ComponentDb) \
                            .filter_by(id=args_component['id']) \
                            .first()
        session.delete(del_component)
        session.commit()
            #return("No se ha podido eliminar: " + args_component['id'])
        return str("Se ha eliminado el component: " + args_component['id']), 204

    def put(self):
        args_component = parser_component.parse_args()
        device_args = args_component['device'].split(';')
        device_add = [session.query(DeviceDb) \
        .filter_by(id=d_id) \
        .first() for d_id in device_args]
        values = {
            'model': args_component['model'], \
            'variable': args_component['variable'], \
            'function':args_component['function'], \
            'state':args_component['state'], \
            'num_pin':args_component['num_pin'] \
            }
        component_update = session.query(ComponentDb) \
        .filter_by(id=args_component['id']).first() 
        component_update.device = device_add
        session.commit()
        return str("se actualizo correctamente")

