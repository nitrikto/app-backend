from flask_restful import reqparse, Resource
from flask import jsonify
from app.models import Device as DeviceDb
from app.models import Cultivo as CultivoDb
from app.common.database import session


parser_device = reqparse.RequestParser()
parser_device.add_argument('id')
parser_device.add_argument('name')
parser_device.add_argument('model')
parser_device.add_argument('marca')
parser_device.add_argument('state')
parser_device.add_argument('cultivo')



class Device(Resource):

    def get(self):
        devices = [c._asdict() for c in session.query(DeviceDb).all()]
        return jsonify(devices)

    def post(self):
        args_device = parser_device.parse_args()
        cultivos_args = args_device['cultivo'].split(';')
        cultivo =[session.query(CultivoDb) \
        .filter_by(id=c_id) \
        .first() for c_id in cultivos_args]
        device = DeviceDb(args_device['name'], \
            args_device['model'], \
            args_device['marca'], \
            args_device['state'])
        device.cultivo = cultivo
        session.add(device)
        session.commit()
        return dict(args_device)

    def delete(self):
        args_device = parser_device.parse_args()
        del_device = session.query(DeviceDb) \
                            .filter_by(id=args_device['id']) \
                            .first()
        session.delete(del_device)
        session.commit()
            #return("No se ha podido eliminar: " + args_device['id'])
        return str("Se ha eliminado el device: " + args_device['id'])

    def put(self):
        args_device = parser_device.parse_args()
        cultivo_args = args_device['cultivo'].split(';')
        cultivo_add = [session.query(CultivoDb) \
        .filter_by(id=c_id) \
        .first() for c_id in cultivo_args]
        values = {
            'name':args_device['name'], \
            'model':args_device['model'], \
            'marca':args_device['marca'], \
            'state':args_device['state'], \
            }
        device_update = session.query(DeviceDb) \
        .filter_by(id=args_device['id']).first() 
        device_update.cultivo = cultivo_add
        session.commit()
        return str("se actualizo correctamente")

