from flask_restful import reqparse, Resource
from flask import jsonify
from app.models import Cultivo as CultivoDb
from app.common.database import session



parser_cultivo = reqparse.RequestParser()
parser_cultivo.add_argument('id')
parser_cultivo.add_argument('name')
parser_cultivo.add_argument('type_cultivo')
parser_cultivo.add_argument('state')

global session

class Cultivo(Resource):

    def get(self):
        cultivos = [c._asdict() for c in session.query(CultivoDb).all()]
        return jsonify(cultivos)

    def post(self):
        args_cultivo = parser_cultivo.parse_args()
        cultivo = CultivoDb(args_cultivo['name'] \
            ,args_cultivo['type_cultivo'] \
            ,args_cultivo['state'])
        session.add(cultivo)
        session.commit()
        return dict(args_cultivo)

    def delete(self):
        args_cultivo = parser_cultivo.parse_args()
        try:
            delete_cultivo = session.query(CultivoDb) \
                            .filter_by(id=args_cultivo['id']) \
                            .first()
            session.delete(delete_cultivo)
            session.commit()
        except:
            return("No se ha podido eliminar: " + args_cultivo['id'])
        return str("Se ha eliminado el cultivo: " + args_cultivo['id'])

    def put(self):
        args_cultivo = parser_cultivo.parse_args()
        values = {'name':args_cultivo['name'], \
         'type_cultivo':args_cultivo['type_cultivo'], \
         'state':args_cultivo['state']}
        session.query(CultivoDb).filter_by(id=args_cultivo['id']) \
        .update(values)
        session.commit()
        return str("se actualizo correctamente")


