from flask import Flask, jsonify, request
from flask_restful import reqparse, Resource, Api, abort
from app.common.database import init_db 
from app.resources.cultivo import Cultivo
from app.resources.device import Device
from app.resources.component import Component
from app.resources.huerta import Huerta, HuertaAll

app = Flask(__name__)
api = Api(app)
init_db()



api.add_resource(Cultivo, '/api/cultivo')
api.add_resource(Device, '/api/device')
api.add_resource(Component, '/api/component')
api.add_resource(Huerta, '/api/huerta/<n_med>')
api.add_resource(HuertaAll, '/api/huerta/all')
